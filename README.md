# Store Norske Leksikon CLI
A simple and small tool to look up articles from [snl.no](https://snl.no/).

## Use
``` text
Usage: snl-cli [OPTIONS] <TERM>

Arguments:
  <TERM>  The term to search for

Options:
  -l, --limit <LIMIT>    Number of results (1-10) [default: 3]
  -o, --offset <OFFSET>  Offset [default: 0]
  -h, --help             Print help
```

## Building
``` text
cargo build -r
```

## Example output
``` text
$ snl-cli -l 10 http
┌──────────────────┬────────┬───────────┬──────────────────────────────────────┐
│ Title            │ ID     │ License   │ URL                                  │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ HTTP             │ 94393  │ fri       │ https://snl.no/HTTP                  │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ HTTP 500         │ 390803 │ fri       │ https://snl.no/HTTP_500              │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ HTTP 404         │ 390802 │ fri       │ https://snl.no/HTTP_404              │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ Det mørke nettet │ 439350 │ fri       │ https://snl.no/Det_m%C3%B8rke_nettet │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ forkortelse      │ 75436  │ begrenset │ https://snl.no/forkortelse           │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ HTTPS            │ 437678 │ begrenset │ https://snl.no/HTTPS                 │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ nettside         │ 135951 │ begrenset │ https://snl.no/nettside              │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ PHP              │ 457701 │ fri       │ https://snl.no/PHP                   │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ SIP              │ 163616 │ begrenset │ https://snl.no/SIP                   │
├──────────────────┼────────┼───────────┼──────────────────────────────────────┤
│ Tim Berners-Lee  │ 39451  │ begrenset │ https://snl.no/Tim_Berners-Lee       │
└──────────────────┴────────┴───────────┴──────────────────────────────────────┘
Found 10 articles in 5.535903175s
```

