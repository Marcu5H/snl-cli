use std::time::Instant;

use clap::Parser;
use reqwest;
use serde::{Deserialize, Serialize};

use colored::*;
use prettytable::format::{FormatBuilder, LinePosition, LineSeparator};
use prettytable::{row, Table};

static SNL_CLI_UA: &str = "snl-cli v0.1.0";

#[derive(Parser)]
struct Query {
    /// Number of results (1-10)
    #[arg(short, long, default_value_t = 3)]
    limit: u8,

    /// Offset
    #[arg(short, long, default_value_t = 0)]
    offset: u8,

    /// The term to search for
    term: String,
}

#[derive(Serialize, Deserialize)]
struct Article {
    id: Option<u8>,
    headword: Option<String>,
    clarification: Option<String>,
    article_type_id: Option<u8>,
    taxonomy_id: Option<u16>,
    taxonomy_title: Option<String>,
    encyclopedia_id: Option<u16>,
    permalink: Option<String>,
    article_id: Option<u32>,
    rank: Option<f32>,
    snippet: Option<String>,
    article_url: Option<String>,
    article_url_json: Option<String>,
    title: Option<String>,
    license: Option<String>,
    first_image_url: Option<String>,
    first_image_license: Option<String>,
    first_two_sentences: Option<String>,
}

#[tokio::main]
async fn main() -> std::result::Result<(), reqwest::Error> {
    let q = Query::parse();

    let request_start = Instant::now();

    let url = format!(
        "https://snl.no/api/v1/search?query={}&limit={}&offset={}",
        q.term, q.limit, q.offset
    );
    let res: Vec<Article> = reqwest::Client::new()
        .get(url)
        .header("User-Agent", SNL_CLI_UA)
        .send()
        .await?
        .json::<Vec<Article>>()
        .await?;

    let found_articles = res.len();

    let mut at = Table::new();
    let style = FormatBuilder::new()
        .column_separator('│')
        .borders('│')
        .separators(&[LinePosition::Top], LineSeparator::new('─', '┬', '┌', '┐'))
        .separators(
            &[LinePosition::Title],
            LineSeparator::new('─', '┼', '├', '┤'),
        )
        .separators(
            &[LinePosition::Intern],
            LineSeparator::new('─', '┼', '├', '┤'),
        )
        .separators(
            &[LinePosition::Bottom],
            LineSeparator::new('─', '┴', '└', '┘'),
        )
        .padding(1, 1)
        .build();

    at.set_format(style);
    at.set_titles(row!["Title", "ID", "License", "URL"]);
    for a in res {
        at.add_row(row![
            a.title.unwrap_or("<Unknown>".to_string()),
            a.article_id.unwrap_or(0),
            a.license.unwrap_or("<Unknown>".to_string()),
            a.article_url.unwrap_or("<Unknown>".to_string())
        ]);
    }

    print!(
        "{}Found {} articles in {}\n",
        at,
        if found_articles > 0 {
            found_articles.to_string().green().bold()
        } else {
            found_articles.to_string().red().bold()
        },
        format!("{:?}", request_start.elapsed())
            .to_string()
            .cyan()
            .bold()
    );

    Ok(())
}
